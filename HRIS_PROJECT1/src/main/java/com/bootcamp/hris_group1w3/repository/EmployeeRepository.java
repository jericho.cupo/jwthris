package com.bootcamp.hris_group1w3.repository;

import com.bootcamp.hris_group1w3.employees.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employees, Integer> {

//    @Query("SELECT e from Employees e where e.Status Like %?1%")
//    List<Employees> status(String status);

    @Query(value = "SELECT * FROM employees e where e.sssnumber = :sss",nativeQuery = true)
    Employees selectBySSS(@Param("sss") Integer sss);

    @Query(value = "SELECT * FROM employees e where e.status = :status",nativeQuery = true)
    List<Employees> selectByStatus(String status);

    @Query(value = "SELECT * from employees e JOIN attendance a ON e.id=a.employees.id WHERE e.sssnumberr = :SSSNumber",nativeQuery = true)
    List<Object> attendanceByEmployeeId(@Param("SSSNumber") Integer sss);

//    @Query(value = "SELECT e from Employees e where e.FirstName like =:firstName",nativeQuery = true)
//    List<Employees> selectBySSSandName(@Param("firstName") String firstName);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM employee_attendance a WHERE a.id in (Select e.id from employees e WHERE e.sssnumber = :SSSNumber)",nativeQuery = true)
    Integer deleteEmployeeBySSS(Integer SSSNumber);
    @Modifying
    @Transactional
    @Query (value = "DELETE  FROM employees WHERE sssnumber = :SSSNumber",nativeQuery = true)
    void deleteEmployee(Integer SSSNumber);


    @Query(value = "SELECT * from employees e where e.first_name like %:firstName% or where e.sssnumber like= :sssNumber",nativeQuery = true)
    List<Employees> selectBySSSandName(String firstName,Integer sssNumber);

    @Query(value="SELECT * FROM employees e WHERE e.sssnumber LIKE %:sssNumber% or e.first_name LIKE %:firstName%",nativeQuery = true)
    List<Employees> getSSSandFname(String firstName,Integer sssNumber);
//    List<Employees> selectBySSSandName(String firstName);


//    @Query("SELECT e from Employees e where e.FirstName like %?1% OR e.SSSNumber=:SSSNumber")
//    List<Employees> searchAllBy(String firstName, Integer SSSNumber);
}
