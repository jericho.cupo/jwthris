package com.bootcamp.hris_group1w3.attendance;

import lombok.Data;

@Data
public class AttendanceResponse {
    private String message;
    private Object payload;

    public AttendanceResponse(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }
}
