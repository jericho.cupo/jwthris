package com.bootcamp.hris_group1w3.attendance;
import com.bootcamp.hris_group1w3.employees.Employees;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor


public class AttendancePK implements Serializable {
    private Integer employees;
    private Date attendanceDate;
}
