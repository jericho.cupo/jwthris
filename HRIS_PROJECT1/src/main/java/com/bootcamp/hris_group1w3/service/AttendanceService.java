package com.bootcamp.hris_group1w3.service;

import com.bootcamp.hris_group1w3.attendance.Attendance;
import com.bootcamp.hris_group1w3.attendance.AttendanceReqBody;
import com.bootcamp.hris_group1w3.employees.Employees;
import com.bootcamp.hris_group1w3.repository.AttendanceRepository;
import com.bootcamp.hris_group1w3.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class AttendanceService {
    @Autowired
    private AttendanceRepository repo;
    @Autowired
    private EmployeeRepository empRepo;

    public Attendance save(AttendanceReqBody att) {

        long millis=System.currentTimeMillis();
        Employees emp = empRepo.getById(att.getId());
        Attendance attendance = new Attendance();
        attendance.setEmployees(emp);
        attendance.setAttendanceDate(new Date(millis));
        attendance.setTimeIn(new java.util.Date());
        attendance.setTimeOut(null);
        attendance.setUpdatedBy(att.getUpdatedBy());
        attendance.setCreatedBy(att.getCreatedBy());
        attendance.setShift(att.getShift());
        attendance.setStatus(att.getStatus());
        attendance.setUpdatedDate(null);
        return repo.save(attendance);
    }


    public List<Attendance> getAttendanceById(Integer id){
        return repo.findAllAttendance(id);

    }
}
