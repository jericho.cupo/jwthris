package com.bootcamp.hris_group1w3.service;

public interface IRoleService<T> extends IService<T> {

	T findByName(String name);
}
