package com.bootcamp.hris_group1w3.employees;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "employees", uniqueConstraints={@UniqueConstraint(columnNames={"SSSNumber"})})
public class Employees  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="firstName",nullable = false, unique = true, length = 50)
    private String FirstName;

    @Column(nullable = false, length = 45)
    private String LastName;

    @Column(nullable = false, length = 45)
    private String MiddleName;

    @Column(nullable = false, length = 45)
    private String Status;

    @Column(nullable = false, length = 45)
    private Integer SSSNumber;

    @Column(nullable = false, length = 45)
    private String CreatedBy;

    @Column(nullable = false, length = 45)
    private Date CreatedDate;

    @Column(nullable = false, length = 45)
    private String UpdatedBy;

    @Column(nullable = false, length = 45)
    private Date UpdatedDate;

    public static Object builder() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Employees employees = (Employees) o;
        return id != null && Objects.equals(id, employees.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
    
    
}
