package com.bootcamp.hris_group1w3.employees;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;

import javax.persistence.Column;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeReqBody {
    private int id;
    private String FirstName;
    private String LastName;
    private String MiddleName;
    private String Status;
    private Integer SSSNumber;
    private String CreatedBy;
    private String UpdatedBy;
}
