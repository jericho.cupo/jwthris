package com.bootcamp.hris_group1w3.controller;


import com.bootcamp.hris_group1w3.employees.EmployeeReqBody;
import com.bootcamp.hris_group1w3.employees.EmployeeResponseBody;
import com.bootcamp.hris_group1w3.employees.Employees;
import com.bootcamp.hris_group1w3.helper.UserNotFoundException;
import com.bootcamp.hris_group1w3.response.OutputResponse;
import com.bootcamp.hris_group1w3.service.AttendanceService;
import com.bootcamp.hris_group1w3.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
//@CrossOrigin(allowedHeaders = "*", origins = "*")
@CrossOrigin(origins = "http://localhost:3000")
public class EmployeeController {
    @Autowired
    private EmployeeService service;
    @Autowired
    private AttendanceService attendanceService;

    @GetMapping("/employeesList")
    public List<Employees> list(){
        return service.listAll();
    }



    @GetMapping(path = "/emp/{sssNumber}/{firstName}")
    public List<Employees> getSSSandFname(@PathVariable String firstName,@PathVariable Integer sssNumber) {
        List<Employees> listEmployees;
        listEmployees = service.getSSSNumberFname(firstName, sssNumber);
        return listEmployees;

    }

    //Get Employee by status

    @GetMapping(path = "/employees")
    public List<Employees> getEmployeesByStatus(@RequestParam("status") String status, @RequestParam("firstName") String firstName, @RequestParam(value = "sssNumber") int sssNumber){
        List<Employees> listEmployees;
        if(status != null){
            listEmployees = service.getByStatus(status);
        }
        else{
            listEmployees = service.getByFnameAndSSSnumber(firstName, sssNumber);
        }
        return listEmployees;

    }

    //Insert employee
    @PostMapping("/employeesList/employee")
    public ResponseEntity<EmployeeResponseBody> saveEmployee(@RequestBody EmployeeReqBody emp){
        Employees employees = service.save(emp);
        return new ResponseEntity<EmployeeResponseBody>(new EmployeeResponseBody("Success",employees), HttpStatus.OK);
    }
    @PutMapping("/employee/update")
    public EmployeeResponseBody updateEmployee(@RequestBody Employees emp){
        emp = service.updateEmployee(emp);

        System.out.println("EMPLOYEEE INFO "+emp);
//        service.save(emp);
        return new EmployeeResponseBody("Updated Successfully",emp);
    }

    @DeleteMapping("/employee/delete/sss/{sssNumber}")
    public String deleteBySSS(@PathVariable(value = "sssNumber") Integer sss)  {

        try {
            Employees e = service.selectBySSS(sss);
            service.delete(e);
            return "Deleted: "+e.getId();
        } catch (UserNotFoundException e) {
            e.printStackTrace();
            return "Nothing to record that match to "+sss;
        }
    }
    @DeleteMapping("/employee/delete/status/{status}")
    public String deleteByStatus(@PathVariable(value = "status") String status)  {
        try {
            Iterable<Employees> e = service.selectBatch(status);
            service.deleteBatch(e);
            List<Integer> ids = new ArrayList<>();
            for (Employees item:
                    e) {
                ids.add(item.getId());
            }
            return "Deleted IDs: "+ids.toString();
        } catch (UserNotFoundException e) {
            e.printStackTrace();
            return "Nothing to record that match to "+status;
        }
    }
    @DeleteMapping("employeeAndAttendance/delete/{SSSNumber}")
    public String deleteEmployeeUsingSSS(@PathVariable("SSSNumber") Integer sssNumber){

        service.deleteEmployeeBySSS(sssNumber);

        return "Deleted Successfully "+ sssNumber;
    }
//    @GetMapping("/employees")
//    public List<Employees> getEmployeeByName(@PathVariable("firstName") String firstName){
//        List<Employees> listEmployees = service.getBySSSandName(firstName);
//        return listEmployees;
//    }

    @GetMapping("/employeesList/employee/{id}")
    public ResponseEntity<Employees> getEmployeeById(@PathVariable Integer id){
        Employees employees = service.findById(id);
        return ResponseEntity.ok(employees);
    }
}
