package com.bootcamp.hris_group1w3.response;

import lombok.Data;

@Data
public class OutputResponse {
    private String message;
    private Object payload;

    public OutputResponse(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }
}