package com.bootcamp.hris_group1w3;

import com.bootcamp.hris_group1w3.employees.Role;
import com.bootcamp.hris_group1w3.employees.User;
import com.bootcamp.hris_group1w3.service.IRoleService;
import com.bootcamp.hris_group1w3.service.IService;
import com.bootcamp.hris_group1w3.utils.ConstantUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class HrisGroup1W3Application implements CommandLineRunner {
    @Autowired
    private IService<User> userService;

    @Autowired
    private IRoleService<Role> roleService;



    public static void main(String[] args) {
        SpringApplication.run(HrisGroup1W3Application.class, args);
    }
    @Override
    public void run(String... args) throws Exception {
        if (roleService.findAll().isEmpty()) {
            roleService.saveOrUpdate(new Role(ConstantUtils.ADMIN.toString()));
            roleService.saveOrUpdate(new Role(ConstantUtils.USER.toString()));
        }

        if (userService.findAll().isEmpty()) {
            User user1 = new User();
            user1.setEmail("test@user.com");
            user1.setName("Test User");
            user1.setMobile("9787456545");
            user1.setRole(roleService.findByName(ConstantUtils.USER.toString()));
            user1.setPassword(new BCryptPasswordEncoder().encode("testuser"));
            userService.saveOrUpdate(user1);

            User user2 = new User();
            user2.setEmail("test@admin.com");
            user2.setName("Test Admin");
            user2.setMobile("9787456545");
            user2.setRole(roleService.findByName(ConstantUtils.ADMIN.toString()));
            user2.setPassword(new BCryptPasswordEncoder().encode("testadmin"));
            userService.saveOrUpdate(user2);
        }


    }



}
