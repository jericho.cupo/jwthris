import React, { Component } from 'react'

class EmployeeList extends Component {
    constructor(props) {
        super(props)

        this.state = {
                employees: []
        }     
       
    }
   
    render() {
        return (
            
            <div>
         
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Status</th>
                                <th scope="col">Updated By</th>
                                <th scope="col">Created By</th>
                                <th scope="col">Updated Date</th>
                                <th scope="col">CreatedDate</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Middle Name</th>
                                <th scope="col">SSS Number</th>
                                <th scope="col">Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.employees.map(
                                        employee => 
                                        <tr key = {employee.id}>
                                            <td>{employee.id}</td>
                                            <td>{employee.status}</td>
                                            <td>{employee.updatedBy}</td>
                                            <td>{employee.createdBy}</td>
                                            <td>{employee.updatedDate}</td>
                                            <td>{employee.createdDate}</td>
                                            <td>{employee.firstName}</td>
                                            <td>{employee.lastName}</td>
                                            <td>{employee.middleName}</td>
                                            <td >{employee.sssnumber}</td>
                                           
                                             
                                        </tr>
                                    )
                                    
                                }

                                
                            </tbody>
                        </table>
                    
                 </div>

            </div>

            
        )

        
    }

    
}




export default EmployeeList;
