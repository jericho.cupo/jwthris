import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import EmployeeList from './components/EmployeeList';
import DeleteEmployees from './components/DeleteEmployees';
import Search_Employee from './components/Search_Employee';

class App extends Component {
  
  render() {

    return (
      <Router>
        <Switch>
          <Route path='/' exact component={Home}/>
          <Route path='/employeeList' exact={true} component={EmployeeList}/>
          <Route path='/searchEmp' exact={true} component={Search_Employee}/>
   
          <Route path='/deleteEmployees' exact={true} component={DeleteEmployees}/>
      
        </Switch>
        </Router>
    
    )
  }
}

export default App;