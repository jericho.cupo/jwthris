import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavbar';

class Home extends Component {
  render() {
    return (
      <div>
        <AppNavbar/>
        <h2>Employee Management System</h2>
      </div>
    );
  }
}

export default Home;