import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { EmployeeListComponent } from './employee-list/employee-list.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { EmployeeService } from './employee.service';
import { EmployeeFilterComponent } from './employee-filter/employee-filter.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { AddAttendanceComponent } from './add-attendance/add-attendance.component';

import { DeleteByStatusComponent } from './delete-by-status/delete-by-status.component';

@NgModule({
  declarations: [
    AppComponent,

    EmployeeListComponent,
     EmployeeFilterComponent,
     AddEmployeeComponent,
     ViewEmployeeComponent,
     AddAttendanceComponent,

     DeleteByStatusComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
