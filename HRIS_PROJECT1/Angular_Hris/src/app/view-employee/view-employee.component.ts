import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AttendanceService } from '../attendance.service';
import { Attendance } from '../attendance';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {

  id!: number;
  employee: Employee = new Employee();
  attendance!: Attendance[];

  constructor(private employeeService: EmployeeService, 
    private attendanceService: AttendanceService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.employeeService.getEmployeeById(this.id).subscribe(data => {
      this.employee = data;
    }, error => console.log(error));

    this.attendanceService.getAttendanceById(this.id).subscribe(data2 => {
      this.attendance= data2;
    }, error => console.log(error));
  }

  goToAddAttendance(){
    this.router.navigateByUrl('/view-employee/'+`${this.id}`+'/add-attendance')
  }

}
