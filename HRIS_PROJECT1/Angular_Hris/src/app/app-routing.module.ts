import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AddAttendanceComponent } from './add-attendance/add-attendance.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteByStatusComponent } from './delete-by-status/delete-by-status.component';

import { EmployeeFilterComponent } from './employee-filter/employee-filter.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';



const routes: Routes = [
  { path: 'employees', component: EmployeeListComponent },
  { path: 'employeesFilter', component: EmployeeFilterComponent },
  { path: 'add-employee', component: AddEmployeeComponent },
  { path: 'view-employee/:id', component: ViewEmployeeComponent },
  { path: 'view-employee/:id/add-attendance', component: AddAttendanceComponent },
  {path: 'delete-by-status',component: DeleteByStatusComponent}

];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  ],
  exports: [RouterModule,
    FormsModule,
    ReactiveFormsModule]
})

export class AppRoutingModule { }
