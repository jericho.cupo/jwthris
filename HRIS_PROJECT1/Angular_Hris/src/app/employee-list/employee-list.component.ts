import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';
import Swal from 'sweetalert2';
import { Observable } from "rxjs";

import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees!: Employee[];

  date = new Date();

  employeeToDelete={
    id:0,
    status:"",
    updatedBy:"",
    createdBy:"",
    updatedDate:Date.toString,
    createdDate:Date.toString,
    firstName:"",
    lastName:"",
    middleName:"",
    sssnumber:0
  };

  employeeToUpdate={
    id:0,
    status:"",
    updatedBy:"",
    createdBy:"",
    updatedDate:Date.toString,
    createdDate:Date.toString,
    firstName:"",
    lastName:"",
    middleName:"",
    sssnumber:0
  };



  del(employee: { id: number; status: string; updatedBy: string; createdBy: string; updatedDate: any; createdDate: any; firstName: string; lastName: string; middleName: string; sssnumber:number; }){
    this.employeeToDelete=employee;
  }
  edit(employee: { id: number; status: string; updatedBy: string; createdBy: string; updatedDate: any; createdDate: any; firstName: string; lastName: string; middleName: string; sssnumber:number; }){
    this.employeeToUpdate=employee;
  }

  

  constructor(private employeeService: EmployeeService) {}


  ngOnInit(){
   
    this.employeeService.getEmployeeList().subscribe(data => {
      this.employees = data;
    });
  }


  //REFRESH
  reloadCurrentPage() {
    window.location.reload();
   }


 

   
  // index = document.getElementById('sss');
  //DELETE by SSS Number
deleteEmployee(employee: { sssnumber: any; }){
  this.employeeService.deleteEmployee(employee.sssnumber).subscribe(
    (resp)=>{
      this.successNotification();
      console.log(resp);
    },(err)=> {
      console.log(err);
    }
  );
}

//UPDATE
updateEmployee(){
  this.employeeService.updateEmployee(this.employeeToUpdate).subscribe(
    (resp)=>{
      console.log(resp);
    },
    (err)=>{
    console.log(err);
    }
  );
}


  successNotification(){
    Swal.fire({
      title: 'Updated Successfully'
    }).then((result) => {
      location.reload();
    });
  }

  deleteNotification(){
    Swal.fire({
      title: 'Employee with SSS no.:'+ this.employeeToDelete.sssnumber+ ' deleted.'
    }).then((result) => {
      location.reload();
    });
  }

}
