import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

import Swal from 'sweetalert2';
import { Employee } from '../employee';



@Component({
  selector: 'app-delete-by-status',
  templateUrl: './delete-by-status.component.html',
  styleUrls: ['./delete-by-status.component.css']
})
export class DeleteByStatusComponent implements OnInit {
  employees!: Employee[];


  constructor(private employeeService: EmployeeService) {}


  ngOnInit(){
   
    this.employeeService.getEmployeeList().subscribe(data => {
      this.employees = data;
    });
  }

status = "Inactive";

employeeToDelete={
  id:0,
  status:"",
  updatedBy:"",
  createdBy:"",
  updatedDate:Date.toString,
  createdDate:Date.toString,
  firstName:"",
  lastName:"",
  middleName:"",
  sssnumber:0
};

  //REFRESH
  reloadCurrentPage() {
    window.location.reload();
   }

   deleteNotification(){
    Swal.fire({
      title: 'All emplyees with '+ this.status+' status successfully deleted.'
    }).then((result) => {
      location.reload();
    });
  }


  
  deleteEmployeeByStatus(){
    this.employeeService.deleteEmployeeByStatus(this.status).subscribe(
      (resp)=>{
        this.alertSucess();
        console.log(resp);
      },(err)=> {
        console.log(err);
      }
    );
  }

  alertSucess(){
    Swal.fire(
      'Success!', 
      'Employee with Inactive status has been deleted!',
      'success',
    );
  }
}
