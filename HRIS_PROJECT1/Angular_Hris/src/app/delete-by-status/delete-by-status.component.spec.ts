import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteByStatusComponent } from './delete-by-status.component';

describe('DeleteByStatusComponent', () => {
  let component: DeleteByStatusComponent;
  let fixture: ComponentFixture<DeleteByStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteByStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteByStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
